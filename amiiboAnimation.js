function amiiboParticleEffect(){
    document.querySelectorAll(".amiibo-particle > img").forEach(ele => {
        document.querySelector(".sound-element").play();
        ele.classList.add("showing")
    })
}

function init(){
    let podium = document.getElementById("podium");

    podium.addEventListener("mouseover", function () {
        let mario = document.querySelector("#mario:not(.collecting)");

        if(mario){
            mario.classList.add("hovered");
            mario.classList.remove("nothovered");
        }
    })

    podium.addEventListener("mouseout", function(){
        let mario = document.querySelector("#mario:not(.collecting)");

        if(mario){
            mario.classList.remove("hovered");
            mario.classList.add("nothovered");
        }  
    })

    podium.addEventListener("click", function(){
        let mario = document.querySelector("#mario:not(.collecting)");

        if(mario){
            mario.classList.add("collecting")
            mario.classList.remove("hovered");
            mario.addEventListener("animationend", amiiboParticleEffect)
            mario.addEventListener("webkitAnimationEnd", amiiboParticleEffect)
        }
    })
}


window.addEventListener('DOMContentLoaded', init);